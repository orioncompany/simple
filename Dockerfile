FROM openjdk:8-jdk

WORKDIR /opt
ADD ./target/simple.jar .

EXPOSE 8080

ENTRYPOINT java -jar ./simple.jar

package ru.volnenko.simple;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.Date;

@WebService
public class App {

    @WebMethod
    public Date getDate() {
        return new Date();
    }

    public static void main(String[] args) {
        System.out.println("WELCOME TO TIME SERVER");
        final String wsdl = "http://0.0.0.0:8080/App?wsdl";
        Endpoint.publish(wsdl, new App());
        System.out.println(wsdl);
    }

}
